import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { SorteioComponent } from './sorteio/sorteio.component';
import { MeusJogosComponent } from './meus-jogos/meus-jogos.component';
import { LeftPadPipe } from './pipes/left-pad.pipe';
import { NovoJogoComponent } from './novo-jogo/novo-jogo.component';

@NgModule({
  declarations: [
    AppComponent,
    SorteioComponent,
    MeusJogosComponent,
    LeftPadPipe,
    NovoJogoComponent
  ],
  imports: [BrowserModule, FormsModule],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
