import { JogoService } from './../services/jogo.service';
import { Component, Input, Output, EventEmitter } from '@angular/core';
import { interval } from 'rxjs';
import { map, take } from 'rxjs/operators';

@Component({
  selector: 'app-sorteio',
  templateUrl: './sorteio.component.html',
  styleUrls: ['./sorteio.component.css']
})
export class SorteioComponent {
  /**
   * Esse dado "chega" de <app-component>
   */
  @Input() numerosSorteados = [];

  /**
   * Esse método é emitido ao componente pai
   * após cada sorteio
   */
  @Output() quandoSortear = new EventEmitter();

  /**
   * Indica que está havendo sorteio
   * e pode ser utilizado para desabilitar
   * componentes, por exemplo. É utilizado
   * no botão para evitar que o usuário
   * acione o sorteio novamente enquanto
   * o mesmo está em andamento.
   */
  sorteando = false;

  /**
   * Injetamos o serviço no construtor
   * @param jogoService Serviço de jogos
   */
  constructor(private jogoService: JogoService) {}

  sortear() {
    /**
     * Indicamos que está havendo
     * sorteio e zeramos os números
     */
    this.sorteando = true;
    this.numerosSorteados = [];

    /**
     * Obtemos os jogos através
     * do service
     */
    const numeros = this.jogoService.obterNovoJogo(1, 60, 6);

    /**
     * Montando o observable:
     *
     * A cada meio segundo, obtemos o tamanho
     * do vetor de números com "take" e utilizamos
     * esse valor para exibir cada número pausadamente
     * pois todos já foram sorteados previamente.
     *
     * O "map" irá obter cada número sorteado, pois
     * vai chegar o índice do vetor (obtido com "take")
     * na variável "index"
     *
     * No subscribe, o observable é executado e cada
     * número será então exibido em tela a cada meio
     * segundo, conforme configuração do observable
     *
     * Por fim (completed), indicamos o fim do
     * sorteio e o botão volta a ficar habilitado
     * para um novo sorteio.
     */
    interval(500)
      .pipe(
        take(numeros.length),
        map(index => numeros[index])
      )
      .subscribe(
        /**
         * Next
         */
        numero => {
          this.numerosSorteados.push(numero);
          this.quandoSortear.emit(this.numerosSorteados);
        },

        /**
         * Error
         */
        erro => {
          console.error(erro);
        },

        /**
         * Completed
         */
        () => {
          this.sorteando = false;
        }
      );
  }
}
