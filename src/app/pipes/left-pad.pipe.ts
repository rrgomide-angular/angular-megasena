import { Pipe, PipeTransform } from '@angular/core';

/**
 * Para criar um pipe no Angular, basta
 * invocar o comando ng g p nome-do-pipe.
 *
 * Assim, o Angular já monta um scaffold
 * do pipe e o código já fica mais orientado e organizado
 * pro desenvolvedor aplicar a lógica necessária
 */
@Pipe({
  name: 'leftPad'
})
export class LeftPadPipe implements PipeTransform {
  /**
   * Transformamos o valor para incluir
   * caracteres à esquerda conforme
   * parâmetros.
   *
   * @param value Valor a ser formatado
   * @param count Quantidade máxima de a ser considerada para formatação
   * @param character Caractere a ser preenchido à esquerda
   */
  transform(value: string, count: number, character?: string): string {
    /**
     * Se não há valor algum,
     * ignoramos
     */
    if (!value) {
      return;
    }

    /**
     * Se não há quantidade,
     * ignoramos
     */
    if (!count) {
      return;
    }

    /**
     * Obtemos a string prefixada
     * com o caractere escolhido
     * pelo desenvolvedor ('0' é o padrão)
     * e concatenamos por fim com o valor
     */
    const leftPad =
      this.getPad(value.toString().length, count, character || '0') + value;

    return leftPad;
  }

  private getPad(valueCount, count, character = '0') {
    if (!count) {
      return '';
    }

    let pad = '';

    for (let i = 0; i < count - valueCount; i++) {
      pad += character;
    }

    return pad;
  }
}
