import { JogoService } from './../services/jogo.service';
import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-novo-jogo',
  templateUrl: './novo-jogo.component.html',
  styleUrls: ['./novo-jogo.component.css']
})
export class NovoJogoComponent implements OnInit {
  /**
   * Método que será emitido
   * ao componente pai
   */
  @Output() enviarNovoJogo = new EventEmitter();

  /**
   * Novo jogo a ser gerado/criado
   */
  novoJogo = [];

  /**
   * Utilizamos o service de jogos
   * no construtor (injeção de dependência)
   * @param jogoService Service de jogos
   */
  constructor(private jogoService: JogoService) {}

  /**
   * No início, já geramos
   * um jogo pra facilitar
   * pro usuário. Esse jogo
   * é editável
   */
  ngOnInit() {
    this.gerarJogo();
  }

  /**
   * Verificamos se o número é passível
   * de ser incluído em um jogo de megasena.
   *
   * O número deve ser válido, estar entre 1 e
   * 60 (inclusive) e deve ser único no vetor
   * de números do jogo
   *
   * @param numero Número a ser verificado
   */
  numeroValido(numero) {
    return !!numero && numero >= 1 && numero <= 60 && this.numeroUnico(numero);
  }

  /**
   * Verifica se determinado número
   * é único a partir do filtro em
   * seu vetor.
   *
   * @param numeroUnico Número a ser verificado no vetor
   */
  private numeroUnico(numeroUnico) {
    const filter = this.novoJogo.filter(numero => numero === numeroUnico);
    return filter.length === 1;
  }

  /**
   * Percorre todos os números do vetor
   * e verifica se pelo menos um deles
   * é inválido.
   */
  temNumeroInvalido() {
    for (let i = 0; i < this.novoJogo.length; i++) {
      const numero = this.novoJogo[i];
      if (!this.numeroValido(numero)) {
        return true;
      }
    }

    return false;
  }

  /**
   * Emite o evento de inclusão de novo jogo.
   * Enviamos o novo jogo já ordenado e de
   * forma imutável
   */
  incluirJogo() {
    this.enviarNovoJogo.emit(this.jogoService.ordenarJogo([...this.novoJogo]));
  }

  /**
   * Gera um novo jogo
   */
  gerarJogo() {
    this.novoJogo = this.jogoService.obterNovoJogo(1, 60, 6);
  }

  /**
   * Esse método auxilia o *ngFor a detectar
   * mudanças, evitando que todo o DOM seja
   * renderizado desnecessariamente.
   *
   * @param index índice do vetor, objeto, etc
   * @param obj valor atual do vetor, objeto, etc.
   */
  trackByIndex(index: number, obj: any) {
    return index;
  }
}
