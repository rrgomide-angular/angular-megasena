import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  /**
   * Aqui, armazenamos os números
   * a serem sorteados
   */
  numerosSorteados = [];

  /**
   * Aqui, guardamos todos os jogos, ou seja,
   * tanto os que são gerados aleatoriamente
   * quanto os jogos criados pelo usuário
   */
  meusJogos = [];

  constructor() {
    // Construtor vazio
  }

  /**
   * Esse método é emitido por <app-sorteio>
   * @param numerosSorteados Números que foram sorteados
   */
  definirSorteio(numerosSorteados) {
    this.numerosSorteados = numerosSorteados;
  }

  /**
   * Esse método é utilizado por cadastrarJogos e
   * também é emitido por <app-novo-jogo>
   * @param novoJogo Jogo a ser inserido
   */
  novoJogo(novoJogo) {
    this.meusJogos.push(novoJogo);
  }

  /**
   * Esse método é emitido por <app-meus-jogos>
   * @param jogos Jogos a serem incluídos
   */
  cadastrarJogos(jogos) {
    /**
     * Limpando os jogos atuais,
     * senão os jogos irão se
     * acumular
     */
    this.meusJogos = [];

    /**
     * Percorrendo o vetor de jogos
     * e criando cada jogo com novoJogo
     */
    jogos.forEach(jogo => this.novoJogo(jogo));
  }
}
