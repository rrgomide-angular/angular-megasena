import { JogoService } from './../services/jogo.service';
import { Component, Input, Output, EventEmitter, OnInit } from '@angular/core';

@Component({
  selector: 'app-meus-jogos',
  templateUrl: './meus-jogos.component.html',
  styleUrls: ['./meus-jogos.component.css']
})
export class MeusJogosComponent implements OnInit {
  /**
   * Os dois inputs abaixo são dados
   * que chegam do componente pai
   */
  @Input() numerosSorteados = [];
  @Input() meusJogos = [];

  /**
   * O output abaixo é um método que será
   * emitido ao componente pai
   *
   */
  @Output() enviarJogos = new EventEmitter();

  /**
   * Utilizamos o service de jogos no construtor
   * @param jogoService Service de jogos
   */
  constructor(private jogoService: JogoService) {}

  ngOnInit() {
    this.novosJogos();
  }

  /**
   * Geramos 4 jogos aleatórios
   * e os emitimos ao componente
   * pai com o auxílio do service
   * de jogos
   */
  novosJogos() {
    const novosJogos = [];
    for (let i = 0; i < 4; i++) {
      novosJogos.push(this.jogoService.obterNovoJogo(1, 60, 6));
    }
    this.enviarJogos.emit(novosJogos);
  }

  /**
   * Aqui verificamos se determinado número
   * foi sorteado
   * @param numero número a ser verificado
   */
  foiSorteado(numero) {
    return this.numerosSorteados.indexOf(numero) !== -1;
  }
}
